// Slideshow    
var dots = document.getElementsByClassName("demodots");
var round = 0;
slideShow();

function slideShow() {
    if (round >= dots.length) {
        round = 0;
    }
    console.log(round);
    showDivs(round);
    round += 1;
    setTimeout(slideShow, 5000);
}

function showDivs(n) {
round = n;
var toReplace = "url('./img/main/parallax" + n + ".jpg')";
    console.log(toReplace);
    var x = document.getElementsByClassName("mainSlideshow");
    var dots = document.getElementsByClassName("demodots");
    for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" w3-white", "");
    }
    x[0].style.backgroundImage = toReplace;
    dots[n].className += " w3-white";
}

// Change style of navbar on scroll
window.onscroll = function() {myFunction()};
function myFunction() {
    var navbar = document.getElementById("myNavbar");
    if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
        navbar.className = "w3-bar" + " w3-card" + " w3-animate-top" + " w3-white";
    } else {
        navbar.className = navbar.className.replace(" w3-card w3-animate-top w3-white", "");
    }
}

// Used to toggle the menu on small screens when clicking on the menu button
function toggleFunction() {
    var x = document.getElementById("navDemo");
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else {
        x.className = x.className.replace(" w3-show", "");
    }
}